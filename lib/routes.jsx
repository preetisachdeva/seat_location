FlowRouter.route("/", {
	name: "Home",
	action(params) {
		renderMainLayoutWith(<Home />);
	}
});

FlowRouter.route("/login", {
	name: "Login",
	action(params) {
		//ReactLayout.render(Login);
		renderMainLayoutWith(<Login />);
	}
});

FlowRouter.route("/register", {
	name: "Register",
	action(params) {
		renderMainLayoutWith(<Register />);
	}
});

FlowRouter.route("/page1", {
	name: "Page1",
	action(params) {
		renderMainLayoutWith(<Page1 />);
	}
});

FlowRouter.route("/page2/:id", {
	name: "Page2",
	action(params) {
		renderMainLayoutWith(<Page2 />);
	}
});

function renderMainLayoutWith(view) {
    ReactLayout.render(MainLayout, {
        header: <Header  />,
        content: view,
        footer: <Footer />
    });
}