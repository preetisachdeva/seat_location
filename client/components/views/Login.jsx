Login = React.createClass({

	mixins: [],
    PropTypes: {

    },
    getInitialState() {
        return {
            errors: {}
        }
    },
    getMeteorData() {
        return {

        }
    },

    // Logout() {
    //     Meteor.logout();
    // },

	onSubmit(event) {
        event.preventDefault();

        var email = $(event.target).find("[name=loginEmail]").val();
        var password = $(event.target).find("[name=loginPassword]").val();

        var errors = {};
        var user = true;

        if (!email) {
            errors.email = "Email required"
        }

        if (!password) {
            errors.password = "Password required"
        }

        if (!user) {
            errors.password = "User not found. Please register!!"
        }

        this.setState({
            errors: errors
        });

        // if (!_.isEmpty(errors)) {
        //     return;
        // }

        Meteor.loginWithPassword(email, password, (err) => {
            if (err) {
                if(err.error == 403){
                    user = false;
                }
            } else {
                FlowRouter.go('Page1');
            }
        });
    },

	render(){
		return(
			<form onSubmit={this.onSubmit}>
			<h1>Login</h1>
		        Email:<input type="email" name="loginEmail" /><p>{this.state.errors.email}</p>
		        Password:<input type="password" name="loginPassword" /><p>{this.state.errors.password}</p>
		        <input type="submit" value="Login" />
                <div> <a href="/register">Sigup!</a></div><p>{this.state.errors.found}</p>
		    </form>
		)
	}
});