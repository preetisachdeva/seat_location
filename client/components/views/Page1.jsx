Page1 = React.createClass({
	mixins: [ReactMeteorData],
    PropTypes: {

    },
    getInitialState() {
        return {
            errors: {}
        };
    },
    handleClick: function(i) {
        var str =  i._str;
        FlowRouter.go('/page2/'+ str);
    },
    getMeteorData() {
    	if(Meteor.userId() !== null) {
	        return {
	        	items: ListCollection.find({}).fetch()
	        }
	    }
    },

    getData() {
    	return this.data.items.map((item) => {
    		return (<div onClick={this.handleClick.bind(this, item._id)} className="boxed" key={item._id}>
                <div className="boxedTeam">
                <span className="teamText">{item.team}</span></div>
                <div className="">
                <span className="managerText">
                    <strong>Manager:</strong>
                {item.manager}</span></div></div>)
    	});
	},
    render(){
		var temp =  this.getData();
		return(
			<div>
			<h1 className="teams">Teams</h1>
			<ul className="overflow">
	        	<div>{temp}</div>
	        </ul>
			</div>
			)
	}
});