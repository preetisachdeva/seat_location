Register = React.createClass({

	mixins: [],
    PropTypes: {

    },
    getInitialState() {
        return {
            errors: {}
        }
    },
    getMeteorData() {
        return {

        }
    },

    // Logout() {
    //     Meteor.logout();
    // },

	onSubmit(event) {
        event.preventDefault();

        var email = $(event.target).find("[name=registerEmail]").val();
        var password = $(event.target).find("[name=registerPassword]").val();

        var errors = {};

        if (!email) {
            errors.email = "Email required"
        }

        if (!password) {
            errors.password = "Password required"
        }

        this.setState({
            errors: errors
        });

        if (!_.isEmpty(errors)) {
            return;
        }
        
        Accounts.createUser({email: email,password: password},function (err) {
        	if (err) {
                this.setState({
                    errors: {'none': err.reason}
                });
                return;
            } else {
                FlowRouter.go('Page1');
            }
        });

    },

	render(){
		return(
			<form onSubmit={this.onSubmit}>
			<h1>Register</h1>
		        Email:<input type="email" name="registerEmail" placeholder="Email"/><p>{this.state.errors.email}</p>
		        Password:<input type="password" name="registerPassword" placeholder="Password"/><p>{this.state.errors.password}</p>
		        <input type="submit" value="Register" />
		    </form>
		)
	}
});
