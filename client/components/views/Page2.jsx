Page2 = React.createClass({
	mixins: [ReactMeteorData],
    PropTypes: {

    },
    getInitialState() {
        return {
        	data: {
        		items: {
        			team_member: []
        		}
        	}
        };
    },
    getMeteorData() {
        if(Meteor.userId() !== null) {
        	var data = {};
        	data.userId = Meteor.userId();
        	data.items = ListCollection.findOne({_id: new Meteor.Collection.ObjectID(FlowRouter.getParam("id"))});
	        return data;
	    }
    },
    handleClick: function(obj) {
    	// var c = confirm("Are you sure to edit seatLocation!");
    	// if (c == true) {
	        var seatLocation = prompt("Please enter seatLocation", obj.seat);
	        if (seatLocation != null) {
	        	var Id = new Meteor.Collection.ObjectID(FlowRouter.getParam("id"));
		        Meteor.call('update', Id, obj.name, seatLocation);
		    }
		//}
    },
    getUpdateBtn (item) {
    	var user = Meteor.user();
    	//console.log(user.emails[0].address);
    	// console.log(item);
    	//console.log(this.data.items.m_email);
    	if ((item.email === user.emails[0].address) || (this.data.items.m_email === user.emails[0].address)) {
    		return (<input type="button" name="seatBtn" value= "Update" onClick={this.handleClick.bind(this,item)} className="buttonRight" />)
    	}
    },
    getData() {
    	//var t = this.getUpdateBtn();
    	return this.data.items.team_member.map((item, i) => {
    		return (<div className = "" key={i}>
    			<div className = "boxedMember">
    			<div className = {(item.designation == "a_manager") ? "a_manager" : "developer"}>{item.name}</div>
    			<div className = "">{item.designation}</div>
    			<div className = ""><input type="text" name="seat" placeholder={item.seat} />{this.getUpdateBtn(item)}</div>
    			</div>
    			</div>)

    	});
    },
	render(){
		var temp = this.getData();
		return(
			<div>
			<h1 className="teams">Team Members</h1>
			<ul className="overflow">
	        	<div>{temp}</div>
	        </ul>
			</div>
			)
	}
});