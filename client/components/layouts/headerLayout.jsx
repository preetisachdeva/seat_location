Header = React.createClass({
	propTypes: {},
	mixins: [ReactMeteorData],

	getInitialState() {
		return {}
	},

	getMeteorData() {
        return {
            currentUser: Meteor.user()
        }
    },

	Logout() {
        Meteor.logout();
        FlowRouter.go('Home');
    },

	render() {
		let loginButton;
        let { currentUser } = this.data;
		if (currentUser) {
            loginButton = (
              <li><a href="#" onClick={this.Logout}>Logout</a></li>
            )
        } else {
            loginButton = (
              <li><a href="/login">Login</a></li>
            )
        }
		return (
			<div className="header">
			<img className="logo" src='/logo.png'/>
			<p className="header1">Daffodil seat Locations</p>
			<div id="loginContainer">
				{loginButton}
          	</div>
			</div>
			)
	}
})